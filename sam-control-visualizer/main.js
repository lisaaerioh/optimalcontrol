var canvasContext = undefined;
var canvas = undefined;
var canvasContainer = undefined;
var animationFrameHandle = undefined;
var lastFrame = new Date();
var botRadius = 10.0;

var initialLocation = undefined;
var locationHistory = undefined;

var k1, k2, k3 = undefined;
var robotType = undefined;
var PIXELS_TO_COORDINATE = 200.0;
var omniBotWheelCenterDist = 1.0;
var omniDirectionalRobotType = "omni-directional";
var diffDriveRobotType = "diff-drive";

var maximalControlsDiffDrive = [
    {
        angular: 1,
        linear: 0
    },
    {
        angular: -1,
        linear: 0
    },
    {
        angular: 0,
        linear: 1
    },
    {
        angular: 0,
        linear: -1
    }
];

const FRAME_RATE = 20.0;


// IMPORTED CODE FOR CLASSMATES
var maximalControlsOmniDirectional = [
    {
        v1: -1.0,
        v2: -1.0,
        v3: -1.0
    },
    {
        v1: 1.0,
        v2: 1.0,
        v3: 1.0
    },
    {
        v1: 1.0,
        v2: -1.0,
        v3: -1.0
    },
    {
        v1: -1.0,
        v2: 1.0,
        v3: -1.0
    },
    {
        v1: -1.0,
        v2: -1.0,
        v3: 1.0
    },
    {
        v1: -1.0,
        v2: 1.0,
        v3: 1.0
    },
    {
        v1: 1.0,
        v2: -1.0,
        v3: 1.0
    },
    {
        v1: 1.0,
        v2: 1.0,
        v3: -1.0
    },
    {
        v1: 1.0,
        v2: 0.0,
        v3: -1.0
    },
    {
        v1: 1.0,
        v2: -1.0,
        v3: 0.0
    },
    {
        v1: 0.0,
        v2: -1.0,
        v3: 1.0
    },
    {
        v1: -1.0,
        v2: 0.0,
        v3: 1.0
    },
    {
        v1: -1.0,
        v2: 1.0,
        v3: 0.0
    },
    {
        v1: 0.0,
        v2: 1.0,
        v3: -1.0
    },
    {
        v1: 0.5,
        v2: 0.5,
        v3: -1.0
    },
    {
        v1: 1.0,
        v2: -0.5,
        v3: -0.5
    },
    {
        v1: 0.5,
        v2: -1.0,
        v3: 0.5
    },
    {
        v1: -0.5,
        v2: -0.5,
        v3: 1.0
    },
    {
        v1: -1.0,
        v2: 0.5,
        v3: 0.5
    },
    {
        v1: -0.5,
        v2: 1.0,
        v3: -0.5
    },
];


function getWheelAngle(botTheta, wheelNumber) {
    return botTheta + (wheelNumber - 1) * 120.0;
}

function toDirectionChangeOmniDirectional(wheelVelocities, position) {
    var v1 = wheelVelocities.v1;
    var v2 = wheelVelocities.v2;
    var v3 = wheelVelocities.v3;

    var theta1 = getWheelAngle(position.theta, 1);
    var theta2 = getWheelAngle(position.theta, 2);
    var theta3 = getWheelAngle(position.theta, 3);

    var s1 = Math.sin(theta1);
    var s2 = Math.sin(theta2);
    var s3 = Math.sin(theta3);

    var c1 = Math.cos(theta1);
    var c2 = Math.cos(theta2);
    var c3 = Math.cos(theta3);

    var xdot = (-s1*v1 -s2*v2 -s3*v3) * 2 / 3;
    var ydot = (c1*v1 + c2*v2 + c3*v3) * 2 / 3;
    var thetadot = (0.5*v1 + 0.5*v2 + 0.5*v3) * 2 / 3; 

    return {
        xdot: xdot,
        ydot: ydot,
        thetadot: thetadot
    };
}

function hamiltonianOmniBot(xdot, ydot, thetadot, position) {
	return k1 * xdot + k2 * ydot + thetadot * (k1 * position.y -k2 * position.x + k3);
}
// END CODE FROM CLASSMATES


function clear(context) {
    context.fillStyle = "#FFFFFF";
    context.fillRect(0, 0, canvas.width, canvas.height); 
}


function setupCanvas() {
    canvas = document.getElementById("visualizer");
    canvasContainer = document.getElementById("visualizer-container");

    canvas.width = canvasContainer.clientWidth;
    canvas.height = canvasContainer.clientWidth;
    canvasContext = canvas.getContext('2d');
}


function toCanvasFrame(position) {
    return {
        x: (position.x * PIXELS_TO_COORDINATE) + canvas.width / 10.0,
        y: (canvas.height / 1.5) - (position.y * PIXELS_TO_COORDINATE),
        theta: position.theta
    }
}


function drawRobot(position, headingPoint) {
    // draw outline
    canvasContext.lineWidth = 3;
    canvasContext.beginPath();
    canvasContext.strokeStyle = "#000000";
    canvasContext.arc(position.x, position.y, botRadius, 0, 2 * Math.PI);
    canvasContext.stroke();

    canvasContext.fillStyle = "#EEEEEE";
    canvasContext.fill();

    // draw center
    canvasContext.beginPath();
    canvasContext.strokeStyle = "#000000";
    canvasContext.arc(position.x, position.y, 1.0, 0, 2 * Math.PI);
    canvasContext.stroke();

    //draw heading indicator
    canvasContext.beginPath();
    canvasContext.strokeStyle = "#00FF00";
    canvasContext.moveTo(position.x, position.y);
    canvasContext.lineTo(
        headingPoint.x,
        headingPoint.y
    );
    canvasContext.stroke();
}


function hamiltonianDiffDrive(xDot, yDot, thetaDot, position) {
    return (k1 * xDot) + (k2 * yDot) + (thetaDot * (k1 * position.y - k2 * position.x + k3));
}


function getControlMaximizingHamiltonian(
    currentPosition,
    hamiltonianFunction,
    maximalControls,
    toDirectionChange
) {
    // select control which maximizes the hamiltonian
    let maximumHamiltonian = Number.NEGATIVE_INFINITY;
    let bestControl = undefined;

    for (var index = 0; index < maximalControls.length; ++index) {
        //console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        var maximalControl = maximalControls[index];
        var asDirectionChange = toDirectionChange(maximalControl, currentPosition);

        currentHamiltonian = hamiltonianFunction(
            asDirectionChange.xdot,
            asDirectionChange.ydot,
            asDirectionChange.thetadot,
            currentPosition
        );

        //console.log('control', maximalControl)
        //console.log('position', currentPosition);
        //console.log('hamiltonian', currentHamiltonian);

        if (currentHamiltonian > maximumHamiltonian) {
            bestControl = maximalControl;
            maximumHamiltonian = currentHamiltonian;
        }
    }

    return bestControl;
}


function chooseNextControlDiffDrive(currentPosition) {
    var nextControl = getControlMaximizingHamiltonian(
        currentPosition,
        hamiltonianDiffDrive,
        maximalControlsDiffDrive,
        toDirectionChangeDiffDrive
    ); 

    return nextControl;
}


function chooseNextControlOmniDirectional(currentPosition) {
    var nextControl = getControlMaximizingHamiltonian(
        currentPosition,
        hamiltonianOmniBot,
        maximalControlsOmniDirectional,
        toDirectionChangeOmniDirectional 
    ); 

    return nextControl;
}


function drawPositionHistory(history) {
    canvasContext.beginPath();
    canvasContext.lineWidth = 3;

    firstPoint = toCanvasFrame(history[0]);

    var index = 0;
    canvasContext.moveTo(firstPoint.x, firstPoint.y);
    for (index = 0; index < history.length; ++index) {
        var currentPt = toCanvasFrame(history[index]); 
        canvasContext.lineTo(currentPt.x, currentPt.y);
    }

    canvasContext.strokeStyle = "#0000FF";
    canvasContext.stroke();
}


function resizeCanvas() {
    var container = document.getElementById("visualizer-container");
    canvas.width = container.clientWidth;
    canvas.height = container.clientHeight;
}


function toDirectionChangeDiffDrive(control, position) {
    return {
        xdot: control.linear * Math.cos(position.theta),
        ydot: control.linear * Math.sin(position.theta),
        thetadot: control.angular
    }
}


function setRobotType() {
    robotType = document.getElementById("bot-type-select").value;
}


function animationLoop() {
    now = new Date();
    sinceLastFrame = (now - lastFrame) / 1000.0;

    if (sinceLastFrame > 1.0 / FRAME_RATE) {
        resizeCanvas();
        clear(canvasContext);

        var currentControl = undefined;
        var currentDirectionChange = undefined;

        var position = locationHistory[locationHistory.length - 1];

        if (robotType == diffDriveRobotType) {
            currentControl = chooseNextControlDiffDrive(locationHistory[locationHistory.length - 1]);
            currentDirectionChange = toDirectionChangeDiffDrive(currentControl, position);
        } else {
            currentControl = chooseNextControlOmniDirectional(
                locationHistory[locationHistory.length - 1]
            );

            currentDirectionChange = toDirectionChangeOmniDirectional(
                currentControl,
                position
            );
        }


        locationHistory.push({
            x: position.x + (currentDirectionChange.xdot * sinceLastFrame),
            y: position.y + (currentDirectionChange.ydot * sinceLastFrame),
            theta: position.theta + (currentDirectionChange.thetadot * sinceLastFrame),
            control: currentControl
        });

        var currentPos = locationHistory[locationHistory.length - 1];
        var headingEnd = {
            x: (Math.cos(currentPos.theta) * botRadius / PIXELS_TO_COORDINATE) + currentPos.x,
            y: (Math.sin(currentPos.theta) * botRadius / PIXELS_TO_COORDINATE) + currentPos.y,
            theta: 0
        }

        drawPositionHistory(locationHistory);
        drawRobot(
            toCanvasFrame(locationHistory[locationHistory.length - 1]),
            toCanvasFrame(headingEnd)
        );
        lastFrame = now;
    }

    window.requestAnimationFrame(animationLoop);
}


function setKValues() {
    k1 = parseFloat(document.getElementById("k1-input").value);
    k2 = parseFloat(document.getElementById("k2-input").value);
    k3 = parseFloat(document.getElementById("k3-input").value);
}

function loadInitialConfiguration() {
    PIXELS_TO_COORDINATE = parseFloat(document.getElementById("pixels-per-coord").value);
    initialLocation = {
        x: 0.0,
        y: 0.0,
        theta: 0.7,
        control: {
            linear: 0.0,
            angular: 0.0
        }
    };

    initialLocation.x = parseFloat(document.getElementById("x-input").value);
    initialLocation.y = parseFloat(document.getElementById("y-input").value);
    initialLocation.theta = parseFloat(document.getElementById("theta-input").value);

    locationHistory = [initialLocation];
}


function main() {
    setKValues();
    loadInitialConfiguration();
    setupCanvas();
    setRobotType();

    animationLoop();
}

$(document).ready(function() {
    main();
});

$(window).resize(function() {
    canvasContainer = document.getElementById("visualizer-container");
    canvas.width = canvasContainer.clientWidth;
    canvas.height = canvasContainer.clientHeight;
});

