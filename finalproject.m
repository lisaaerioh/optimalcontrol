%Project
k1 = 1;
k2 = 0;
k3 = 0;
x(1)=0;
y(1)=0;
theta(1) = 0.7;
n = 1;
controls = [1,0;0,1;0,-1;-1,0];
for t = 0:.1:30
    H = 0;
    for i = 1:4
        v = controls(i,1);
        w = controls(i,2);
        xtrydot = v*cos(theta(n));
        ytrydot = v*sin(theta(n));
        thetatrydot = w;
        temp = k1*xtrydot+k2*ytrydot+thetatrydot*(k1*y(n)-k2*x(n)+k3);
        if temp > H
            H = temp;
            xdot = xtrydot;
            ydot = ytrydot;
            thetadot = thetatrydot;
        end;
    end;
    n;
    n = n+1;
    x(n) = x(n-1)+xdot*t;
    y(n) = y(n-1)+ydot*t;
    temp2 = theta(n-1)+thetadot*t;
    if (temp2 < 2*pi) && (temp2 > (-2*pi))
        theta(n) = theta(n-1)+thetadot*t;
    else
        theta(n) = 0;
    end;
    
end;
plot(x,y);
% axis([0 2 0 1]);