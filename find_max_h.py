for (v, w) in [(1,0),(0,1),(-1,0),(0,-1)]:
        # convert x_dot y_dot  theta_dot to v, w
        x_dot = v * cos(theta)
        y_dot = v * sin(theta)
        theta_dot = w
        H = k1 * x_dot + k2 * y_dot + theta_dot * (k1 * y - k2* x + k3)
        print(H)
        print(v,w)
        if H > max_H:
                max_H = H
                max_v_w = (v,w)
