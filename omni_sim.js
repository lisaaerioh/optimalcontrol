
var wheel_center_dist=1;
var k1=1;
var k2=0;
var k3=0;
var timestep = 1;
var vsw=calc_vsw(max_V);

var max_V=[[-1,-1,-1],[1,1,1],[1,-1,-1],[-1,1,-1],[-1,-1,-1],[-1,1,1],[1,-1,1],[1,1,-1],[,1,0,-1],[1,-1,0],[0,-1,1],[-1,0,1],[-1,1,0],[0,1,-1],[.5,.5,-1],[1,-.5,-.5],[.5,-1,.5],[-.5,-.5,1],[-1,.5,.5],[-.5,1,-.5]];

function omni_sim(config){

	var maxvsw=calc_Max_Ham(vsw,config);
	config= update_pos(maxvsw[1],config,timestep);
	return config;

}


function update_pos(q_dot,pos,timestep){
	pos[0]=pos[0]+timestep*q_dot[0];
	pos[1]=pos[1]+timestep*q_dot[1];
	pos[2]=pos[2]+timestep*q_dot[2];
	
	return pos
}


function calc_Max_Ham(vsw,pos)

	
	var q_dot=cald_q_dot(vsw[0],pos[2]);
	var ham=calc_Ham(q_dot,pos);
	var maxvsw=[ham,q_dot]
	for(index=1;index<vsw.length;++index){
		var q_dot=cald_q_dot(vsw[index],pos[2]);
		var ham=calc_Ham(q_dot,pos);

		if(maxvsw[0]<ham){
			maxvsw[0]=ham;
			maxvsw[1]=q_dot;
		}
	}
	return maxvsw;

function calc_Ham(dot,pos){
	return k1*dot[0]+k2*dot[1]+dot[2]*(k1*pos[1]-k2*pos[0]+k3);

}


function calc_vsw(wheel_velocities){
	
	var vswarray=[]
	for (index=0;index<wheel_velocities.length;++index){
		vswarray.push(calc_vswHelper(wheel_velocities[index]));
	}
	return vswarray;
}

function calc_vwsHelper(v){
	var v=Math.sqrt(3)/3.*(v[2]-v[0]);
	var s=(1/3.)*(v[2]+v[0])-(2/3.)*v[1];
	var w=(1/(3*wheel_center_dist))*(v[0]+v[1]+v[2]);
	return [v,s,w];
}

function calc_q_dot(vsw,theta){
	
	var x_dot=vsw[0]*Math.cos(theta)-vsw[1]*Math.sin(theta);
	var y_dot=vsw[0]*Math.sin(theta)+vsw[1]*Math.cos(theta);
	var theta_dot=vsw[2];
	return [x_dot,y_dot,theta_dot];



}