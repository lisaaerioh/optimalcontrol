import math
import matplotlib.pyplot as plt

class OptimalTrajectory:
    def __init__(self, k1, k2, k3, x, y, theta):
        self.k1 = k1
        self.k2 = k2
        self.k3 = k3
        self.x = x
        self.y = y
        self.theta = theta
        self.control_list = [(1, 0), (-1, 0), (0, 1), (0, -1)]
        self.control = self.get_init_control()

    def get_init_control(self):
        max_H = float('-inf')
        max_v_w = None
        for (v, w) in self.control_list:
            # convert x_dot y_dot  theta_dot to v, w
            x_dot = v * math.cos(self.theta)
            y_dot = v * math.sin(self.theta)
            theta_dot = w
            H = self.k1 * x_dot + self.k2 * y_dot + theta_dot * (self.k1 * self.y - self.k2 * self.x + self.k3)
            # print(H)
            # print(v, w)

            if H > max_H:
                max_H = H
                max_v_w = (v, w)

        return max_v_w

    def get_time_w(self):
        print("W")
        m = self.k1 * self.y - (self.k2 * self.x) + self.k3
        # print("M: " + str(m))
        a = self.k1**2 + self.k2**2
        # print("A: " + str(a))
        b = 2 * m * self.k2
        # print("B: " + str(b))
        b2 = -b
        c = m**2 - self.k1**2
        # print("C: " + str(c))

        q1 = (-b + math.sqrt(b**2 - 4 * a * c)) / (2 * a)
        # print("Q1: " + str(q1))
        q2 = (-b - math.sqrt(b**2 - 4 * a * c)) / (2 * a)
        # print("Q2: " + str(q2))
        q3 = (-b2 + math.sqrt(b2**2 - 4 * a * c)) / (2 * a)
        q4 = (-b2 - math.sqrt(b2**2 - 4 * a * c)) / (2 * a)

        t1 = math.asin(q1) - self.theta
        t2 = math.asin(q2) - self.theta
        t3 = math.asin(q3) - self.theta
        t4 = math.asin(q4) - self.theta

        print("T1: " + str(t1) + " T2: " + str(t2) + " T3: " + str(t3) + " T4: " + str(t4))
        return t1, t2

    def get_time_ctrl_w(self):
        t1, t2 = self.get_time_w()

        if t1 == 0:
            t_min = abs(t2)
        elif t2 == 0:
            t_min = abs(t1)

        else:
            t_min = max(abs(t1), abs(t2))

        next_control = self.control_list[0]
        # print("TMIN: " + str(t_min))
        # print("TEST: " + str(-t2))

        if t_min == t2 or t_min == -t2:
            next_control = self.control_list[1]

        return next_control, t_min

    def get_time_ctrl_negw(self):
        t1, t2 = self.get_time_w()

        if t1 == 0:
            t_min = abs(t2)
        elif t2 == 0:
            t_min = abs(t1)

        else:
            t_min = max(abs(t1), abs(t2))

        next_control = self.control_list[1]
        print("TMIN: " + str(t_min))
        print("TEST: " + str(-t2))
        if t_min == t2 or t_min == -t2:
            next_control = self.control_list[0]

        return next_control, t_min

    def get_time_v(self):
        print("V")
        n1 = -self.k3 - (self.k1 * math.cos(self.theta)) - (self.k2 * math.sin(self.theta))\
            - (self.k1 * self.y) + (self.k2 * self.x)
        d1 = (self.k1 * math.sin(self.theta)) - (self.k2 * math.cos(self.theta))

        t1 = n1/d1

        n2 = self.k3 - (self.k1 * math.cos(self.theta)) - (self.k2 * math.sin(self.theta))\
            + (self.k1 * self.y) - (self.k2 * self.x)
        d2 = -(self.k1 * math.sin(self.theta)) + (self.k2 * math.cos(self.theta))

        t2 = n2/d2
        return t1, t2

    def get_time_ctrl_v(self):
        t1, t2 = self.get_time_v()
        print("T1: " + str(t1) + " T2: " + str(t2))
        next_control = None
        time = None

        if t1 < 0:
            next_control = self.control_list[2]
            time = abs(t2)
        elif t2 < 0:
            next_control = self.control_list[3]
            time = abs(t1)

        return next_control, time

    def get_time_ctrl_negv(self):
        t1, t2 = self.get_time_v()
        print("T1: " + str(t1) + " T2: " + str(t2))
        next_control = None
        time = None

        if t1 < 0:
            next_control = self.control_list[3]
            time = abs(t2)
        elif t2 < 0:
            next_control = self.control_list[2]
            time = abs(t1)

        return next_control, time

    def get_time_next_ctrl(self):
        if self.control == self.control_list[0]:
            next_control, t = self.get_time_ctrl_v()

        elif self.control == self.control_list[1]:
            next_control, t = self.get_time_ctrl_negv()

        elif self.control == self.control_list[2]:
            next_control, t = self.get_time_ctrl_w()

        else:
            next_control, t = self.get_time_ctrl_negw()

        return next_control, t

    def update(self, next_control, t):
        if self.control == self.control_list[0]:
            self.x = self.x + t * math.cos(self.theta)
            self.y = self.y + t * math.sin(self.theta)

        elif self.control == self.control_list[1]:
            self.x = self.x - t * math.cos(self.theta)
            self.y = self.y - t * math.sin(self.theta)

        elif self.control == self.control_list[2]:
            self.theta = self.theta + t

        else:
            self.theta = self.theta - t

        self.control = next_control


        print('NEW X: ' + str(self.x))
        print('NEW Y: ' + str(self.y))
        print('NEW THETA: ' + str(self.theta))
        # print('NEW CONTROL: ' + str(self.control))

if __name__ == "__main__":
    test = OptimalTrajectory(1, 0, 0, 0, 0, 0.7)
    x = []
    y = []
    theta = []
    for i in range(0, 8):
        next, t = test.get_time_next_ctrl()
        print("TIME: " + str(t))
        print("NEXT: " + str(next))
        test.update(next, t)
        x.append(test.x)
        y.append(test.y)
        theta.append(test.theta)

    fig1 = plt.figure()
    plt.plot(x, y, marker = 'o', color = 'red')
    plt.show()
